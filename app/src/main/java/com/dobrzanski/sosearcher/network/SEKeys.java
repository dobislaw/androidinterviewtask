package com.dobrzanski.sosearcher.network;

/**
 * Constant stack exchange API keys.
 */
public class SEKeys {

    public static final String KEY_ITEMS = "items"; // main array

    public static final String KEY_OWNER = "owner"; // owner object

    public static final String KEY_PROFILE_IMAGE = "profile_image";

    public static final String KEY_DISPLAY_NAME = "display_name";

    public static final String KEY_TITLE = "title";

    public static final String KEY_ANSWER_COUNT = "answer_count";

    public static final String KEY_LINK = "link";
}
