package com.dobrzanski.sosearcher.network;

import android.util.Log;

/**
 * Stack exchange API handling:
 */
public class SEConstants {

    public static final String TAG = SEConstants.class.getSimpleName();

    public static String notAvailable = "N/A";

    public static String stackOverflowQuery =
            "https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity";

    public static String stackOverflowSearchByTag(String tag) {
        String query = stackOverflowQuery + "&tagged=" + tag + "&site=stackoverflow";
        Log.d(TAG,query);
        return query;
    }
}
