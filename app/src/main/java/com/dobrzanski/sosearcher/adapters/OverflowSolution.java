package com.dobrzanski.sosearcher.adapters;

/**
 * Class for storing information concerning the stack overflow solution
 */
public class OverflowSolution {

    private int answersCount;
    private String userName, issueTitle, issueUrl, profileUrl;

    public OverflowSolution(String userName, String issueTitle,
                            String issueUrl, String profileUrl, int answersCount) {
        this.userName = userName;
        this.issueTitle = issueTitle;
        this.issueUrl = issueUrl;
        this.profileUrl = profileUrl;
        this.answersCount = answersCount;
    }

    public int getAnswersCount() {
        return answersCount;
    }

    public void setAnswersCount(int answersCount) {
        this.answersCount = answersCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIssueTitle() {
        return issueTitle;
    }

    public void setIssueTitle(String issueTitle) {
        this.issueTitle = issueTitle;
    }

    public String getIssueUrl() {
        return issueUrl;
    }

    public void setIssueUrl(String issueUrl) {
        this.issueUrl = issueUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    @Override
    public String toString() {
        return userName + ", " + issueTitle + ", "
                + answersCount +  ",\n IssueURL: " + issueUrl + ",\n ProfileURL: " + profileUrl;
    }
}
