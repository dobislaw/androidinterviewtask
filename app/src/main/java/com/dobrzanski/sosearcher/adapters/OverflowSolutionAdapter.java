package com.dobrzanski.sosearcher.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.dobrzanski.sosearcher.R;
import com.dobrzanski.sosearcher.network.SEConstants;
import com.dobrzanski.sosearcher.network.VolleySingleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for recyclerList
 */
public class OverflowSolutionAdapter extends RecyclerView.Adapter<OverflowSolutionAdapter.OverflowSolutionViewHolder> {

    private Context context;

    private LayoutInflater inflater;

    // networking:
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;

    // adapter's own list of solutions for mapping.
    private List<OverflowSolution> overflowSolutionList = new ArrayList<>();

    public OverflowSolutionAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.volleySingleton = VolleySingleton.getInstance();
        this.imageLoader = volleySingleton.getImageLoader();
    }


    @Override
    public OverflowSolutionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // assign our xml layout for this partivular holder
        View view = inflater.inflate(R.layout.overflow_solution_item,parent,false);
        return new OverflowSolutionViewHolder(view);
    }

    /**
     * ViewHolder gets updated with contents of my list elements
     */
    @Override
    public void onBindViewHolder(final OverflowSolutionViewHolder holder, int position) {
        OverflowSolution overflowSolution = overflowSolutionList.get(position);

        holder.issueTitleTV.setText(overflowSolution.getIssueTitle());
        holder.userNameTV.setText(overflowSolution.getUserName());

        // error handling included

        int answersCount = overflowSolution.getAnswersCount();
        if (answersCount == -1) {
            holder.answersCountTV.setText("");
            holder.answersCountTV.setAlpha(0.2f);
        } else {
            holder.answersCountTV.setText(String.valueOf(answersCount));
        }

        // get url for an avatar:
        String profileUrl = overflowSolution.getProfileUrl();
        // load image:
        loadImage(holder,profileUrl);
    }

    /**
     * Load image with proper error checking
     */
    private void loadImage(final OverflowSolutionViewHolder holder, String profileUrl) {
        if (!profileUrl.equals(SEConstants.notAvailable)) {
            imageLoader.get(profileUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    // set the image:
                    holder.avatarIconIV.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    // set dummy image (from android original library)
                    holder.avatarIconIV.setImageDrawable(context.getDrawable(android.R.drawable.ic_delete));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return overflowSolutionList.size();
    }

    /**
     *  A ViewHolder describes an item view and metadata about its place within the RecyclerView.
     */
    class OverflowSolutionViewHolder extends RecyclerView.ViewHolder {

        public TextView issueTitleTV, userNameTV, answersCountTV;
        public ImageView avatarIconIV;

        public OverflowSolutionViewHolder(View itemView) {
            super(itemView);
            // get refs to my xml widgets
            issueTitleTV = (TextView) itemView.findViewById(R.id.data_item_issue);
            userNameTV = (TextView) itemView.findViewById(R.id.data_item_user);
            answersCountTV = (TextView) itemView.findViewById(R.id.data_item_answers);
            avatarIconIV = (ImageView) itemView.findViewById(R.id.data_item_avatar_icon);
        }
    }

    public void setOverflowSolutionList(List<OverflowSolution> list) {
        this.overflowSolutionList = list;
        // UI update:
        notifyItemRangeChanged(0,list.size());
    }
}
