package com.dobrzanski.sosearcher.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.dobrzanski.sosearcher.R;

/**
 * Created by dobi on 24.01.16.
 */
public class SearchFragment extends Fragment {

    public static final String TAG = SearchFragment.class.getSimpleName();

    private EditText inputRequestEditText;
    private TextInputLayout inputLayoutQuery;
    private Button clearButton;

    public SearchFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment,container,false);

        clearButton = (Button) view.findViewById(R.id.clear_button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputRequestEditText.setText("");
            }
        });

        inputLayoutQuery = (TextInputLayout)view.findViewById(R.id.input_layout_query);
        inputRequestEditText = (EditText)view.findViewById(R.id.input_query);
        inputRequestEditText.addTextChangedListener(new MyTextWatcher(inputRequestEditText));
        return view;
    }

    /**
     * TODO.
     */
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_query:
                    validateQuery();
                    break;
            }
        }
    }

    private boolean validateQuery() {
        if (inputRequestEditText.getText().toString().trim().isEmpty()) {
            inputLayoutQuery.setError(getString(R.string.err_msg_query));
            requestFocus(inputRequestEditText);
            return false;
        } else {
            inputLayoutQuery.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public String getStringRequest() {
        return inputRequestEditText.getText().toString();
    }

}
