package com.dobrzanski.sosearcher.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.dobrzanski.sosearcher.R;
import com.dobrzanski.sosearcher.adapters.OverflowSolution;
import com.dobrzanski.sosearcher.adapters.OverflowSolutionAdapter;
import com.dobrzanski.sosearcher.network.SEKeys;
import com.dobrzanski.sosearcher.network.VolleySingleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dobi on 24.01.16.
 */
public class ResultsFragment extends Fragment {

    public static final String TAG = ResultsFragment.class.getSimpleName();

    // UI:
    private RecyclerView recyclerView;
    private OverflowSolutionAdapter overflowSolutionAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noDataTextView;

    // data:
    private List<OverflowSolution> overflowSolutionList = new ArrayList<>();

    public ResultsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.results_fragment,container,false);

        noDataTextView = (TextView) view.findViewById(R.id.no_data_text_view);

        recyclerView = (RecyclerView)view.findViewById(R.id.results_recycler_view); // gets reference to recycler view
        overflowSolutionAdapter = new OverflowSolutionAdapter(getActivity());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // set the adapter:
        recyclerView.setAdapter(overflowSolutionAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // on click works as master-detail pattern
                // descendant navigation
                // http://developer.android.com/training/implementing-navigation/descendant.html
                // invoke new intent
                Intent intent = new Intent(getActivity(), DetailsFragmentActivity.class);
                intent.putExtra(SEKeys.KEY_LINK, overflowSolutionList.get(position).getIssueUrl());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                startActivity(intent);
            }
        }));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        return view;
    }

    /**
     * Data refreshing:
     * http://sapandiwakar.in/pull-to-refresh-for-android-recyclerview-or-any-other-vertically-scrolling-view/
     */
    private void refreshData() {
        // load new content:
        ((MainActivity)getActivity()).sendJSONRequest();
        overflowSolutionAdapter.notifyDataSetChanged();

        // content is loaded:
        onDataLoadComplete();
    }

    private void onDataLoadComplete() {
        swipeRefreshLayout.setRefreshing(false);
    }


    /**
     * Recycler view has problems with onClickListener
     * http://www.androidhive.info/2016/01/android-working-with-recycler-view/
     */
    public interface ClickListener {
        void onClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ResultsFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ResultsFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public void setOverflowSolutionList(List<OverflowSolution> overflowSolutionList) {
        // set to the fragment:
        this.overflowSolutionList = overflowSolutionList;
        // and to the adapter:
        this.overflowSolutionAdapter.setOverflowSolutionList(this.overflowSolutionList);
        // ui update
        noDataTextView.setVisibility(View.INVISIBLE);
    }

    /**
     * Very cool one.
     * http://stackoverflow.com/questions/10024739/how-to-determine-when-fragment-becomes-visible-in-viewpager
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            if (overflowSolutionList.isEmpty())
//                noDataTextView.setVisibility(View.VISIBLE);
//        }
    }
}
