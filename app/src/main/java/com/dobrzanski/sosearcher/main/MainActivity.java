package com.dobrzanski.sosearcher.main;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dobrzanski.sosearcher.R;
import com.dobrzanski.sosearcher.adapters.OverflowSolution;
import com.dobrzanski.sosearcher.network.SEConstants;
import com.dobrzanski.sosearcher.network.SEKeys;
import com.dobrzanski.sosearcher.network.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    // UI controls:
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FloatingActionButton fab;
    private CoordinatorLayout coordinatorLayout;

    // fragment reference;
    private ResultsFragment resultsFragment;
    private SearchFragment searchFragment;

    private ViewPagerAdapter viewPagerAdapter;
    private ProgressDialog progressDialog;

    // JSON request:
    private String currentRequestArgs;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_activity_layout);

        // toolbar:
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        // disable this functionality
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);

        // view pager + viewPagerAdapter for fragments:
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        // 2 main tabs:
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        // Progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading content...");

        // FAB:
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (viewPager.getCurrentItem()) {
                    case 0:
                        // before requesting JSON response, get what is in the text field.
                        currentRequestArgs = searchFragment.getStringRequest();

                        progressDialog.show();
                        sendJSONRequest();
                        break;
                    case 1:
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                        break;
                }
            }
        });
    }

    /**
     * Actual request making + queuing it.
     */
    public void sendJSONRequest() {

        String tag_json_obj = "json_obj_req";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                SEConstants.stackOverflowSearchByTag(currentRequestArgs),
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        // populate the list in a proper fragment
                        resultsFragment.setOverflowSolutionList(parseJSONResponse(response));

                        // if the response is present, switch instantly to the results fragment
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);

                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        // hide the progress dialog
                        progressDialog.hide();
                        handleError(error);
                    }
        });
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void handleError(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            // http://www.androidhive.info/2015/09/android-material-design-snackbar-example/
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, R.string.err_msg_timeout, Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            sendJSONRequest();
                        }
                    });
            snackbar.show();
        } else if (error instanceof NetworkError) {
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, R.string.err_msg_network, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }


    private List<OverflowSolution> parseJSONResponse(JSONObject response) {
        List<OverflowSolution> overflowSolutionList = new ArrayList<>();
        if (response != null && response.length() > 0)   {
            try {
                // handle key and value checking here.
                // default values:
                String userName = SEConstants.notAvailable;
                String issueTitle = SEConstants.notAvailable;
                String profileUrl = SEConstants.notAvailable;
                String issueUrl = SEConstants.notAvailable;
                int answerCount = -1;

                if (response.has(SEKeys.KEY_ITEMS)) {
                    JSONArray arrayItems = response.getJSONArray(SEKeys.KEY_ITEMS);
                    for (int i = 0; i < arrayItems.length(); i++) {
                        JSONObject item = arrayItems.getJSONObject(i);

                        // Get owner object data:
                        JSONObject owner = item.getJSONObject(SEKeys.KEY_OWNER);
                        if (owner.has(SEKeys.KEY_DISPLAY_NAME) & !owner.isNull(SEKeys.KEY_DISPLAY_NAME)) {
                            // get userNameTV name
                            userName = owner.getString(SEKeys.KEY_DISPLAY_NAME);
                        }
                        if (owner.has(SEKeys.KEY_PROFILE_IMAGE) & !owner.isNull(SEKeys.KEY_PROFILE_IMAGE)) {
                            // get profile image
                            profileUrl = owner.getString(SEKeys.KEY_PROFILE_IMAGE);
                        }

                        // Get the rest (i.e. answer_count + title + link):
                        if (item.has(SEKeys.KEY_LINK) & !item.isNull(SEKeys.KEY_LINK)) {
                            issueUrl = item.getString(SEKeys.KEY_LINK);
                        }

                        if (item.has(SEKeys.KEY_TITLE) & !item.isNull(SEKeys.KEY_TITLE)) {
                            issueTitle = item.getString(SEKeys.KEY_TITLE);
                        }

                        if (item.has(SEKeys.KEY_ANSWER_COUNT) & !item.isNull(SEKeys.KEY_ANSWER_COUNT)) {
                            answerCount = item.getInt(SEKeys.KEY_ANSWER_COUNT);
                        }

                        OverflowSolution overflowSolution = new OverflowSolution(
                                userName, issueTitle, issueUrl, profileUrl, answerCount);

                        // debugging:
                        Log.d(TAG,overflowSolution.toString());

                        // populate the list here under certain conditions
                        // title and and link are obligatory!
                        if (!issueTitle.equals(SEConstants.notAvailable)
                                & !issueUrl.equals(SEConstants.notAvailable))
                            overflowSolutionList.add(overflowSolution);
                    }
                }
            } catch (JSONException e) {

            }
        }
        return overflowSolutionList;
    }

    private void setupViewPager(ViewPager viewPager) {
        // get this reference:
        resultsFragment = new ResultsFragment();
        searchFragment = new SearchFragment();

        // http://stackoverflow.com/questions/11182180/understanding-fragments-setretaininstanceboolean
        resultsFragment.setRetainInstance(true);
        searchFragment.setRetainInstance(true);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(searchFragment, "Search");
        viewPagerAdapter.addFragment(resultsFragment, "Results");
        viewPager.setAdapter(viewPagerAdapter);
    }

    /**
     * an viewPagerAdapter which is storing fragment references in a list.
     * https://guides.codepath.com/android/ViewPager-with-FragmentPagerAdapter
     * Used 'state' in order to hold the state when for example the device screen rotates
     */
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.developer_info:
                AlertDialog.Builder infoDialog  = new AlertDialog.Builder(this);
                infoDialog.setMessage("will code for food.");
                infoDialog.setTitle("Michał Dobrzański");
                infoDialog.setPositiveButton("That\'s right!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplication(),"A truly splendid answer, sir!",Toast.LENGTH_SHORT).show();
                    }
                });
                infoDialog.setNegativeButton("Maybe?", null);
                infoDialog.setCancelable(true);
                infoDialog.show();
                return true;
            default:
                return true;
        }
    }

}
