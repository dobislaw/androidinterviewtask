package com.dobrzanski.sosearcher.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.dobrzanski.sosearcher.R;
import com.dobrzanski.sosearcher.network.SEKeys;

/**
 * Created by dobi on 24.01.16.
 */
public class DetailsFragmentActivity extends FragmentActivity {

    public static final String TAG = DetailsFragmentActivity.class.getSimpleName();


//    public DetailsFragmentActivity() {
//    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the progress bar:
        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.details_fragment_activity);

        getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

        Intent intent = getIntent();
        String issueUrl = intent.getStringExtra(SEKeys.KEY_LINK); // passed issueTitleTV url
        Log.d(TAG, "My url: " + issueUrl);

        WebView webView = (WebView) findViewById(R.id.details_webpage);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);

        // load page:
        webView.loadUrl(issueUrl);

        // set new chrome client
        /**
         * http://www.firstdroid.com/2010/08/04/adding-progress-bar-on-webview-android-tutorials/
         */
        webView.setWebChromeClient(new WebChromeClient());
    }



}
